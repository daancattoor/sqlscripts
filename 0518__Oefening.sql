USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE Huisdieren ADD COLUMN Geluid VARCHAR(20) CHARSET utf8mb4;
UPDATE Huisdieren SET Geluid = "WAF!" WHERE Soort = 'hond';
UPDATE Huisdieren SET Geluid = "miauwww..." WHERE Soort = 'kat';
SET SQL_SAFE_UPDATES = 0;
 