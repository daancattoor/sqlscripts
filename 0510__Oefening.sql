USE ModernWays;
INSERT INTO Liedjes(titel, artiest, album, jaar)
VALUES('Stairway to Heaven' , 'Led Zeppelin' , 'Led Zeppelin IV', '1971'),
('Good Enough' , 'Molly Tuttle' , 'Rise' , '2017'),
('Outrage for the Execution of Willie McGee' , 'Goodnight, Texas' , 'Conductor' , '2018'),
('They Lie' , 'Layla Zoe' , 'The Lily' , '2013'),
('It Aint You' , 'Danielle Nicole' , 'Wolf Den' , '2015'),
('Unchained' , 'Van Halen' , 'Fair Warning' , '1981')