USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE liedjes ADD COLUMN Genre VARCHAR(20) CHARSET utf8mb4;
UPDATE liedjes SET Genre = 'Hardrock' WHERE Artiest = 'Led Zeppelin' OR Artiest = 'Van Halen';
SET SQL_SAFE_UPDATES = 0;
