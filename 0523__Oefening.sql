USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Leeftijd = 9 WHERE Soort = 'hond' AND Baasje = 'Christiane'
	OR Soort = 'kat' AND Baasje = 'Bert';
SET SQL_SAFE_UPDATES = 1;