USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
DELETE FROM huisdieren WHERE Soort = 'hond' AND Baasje = 'Thaïs'
	OR Soort = 'kat' AND Baasje = 'Truus';
SET SQL_SAFE_UPDATES = 1;